# Blue Green Deployment

This project is set up to use a Blue Green deployment strategy, which allows for zero downtime when deploying new code to production. Blue Green deployment involves maintaining two identical environments - one that is live and serving traffic (the blue environment) and one that is not yet live (the green environment). When deploying new code, the new version is deployed to the green environment and tested thoroughly. Once the green environment is deemed stable, traffic is switched over to the green environment and the blue environment becomes the new green environment.
<br />
<br />

# Deployment Process

The deployment process is automated using GitLab CI/CD pipelines. When a new commit is pushed to the repository, a new deployment is triggered. The `CI_COMMIT_SHORT_SHA` environment variable is used to create a unique identifier for each deployment.

When deploying the project, the depRegion and deployment.word variables are set to `${CI_COMMIT_SHORT_SHA}` . The release name is set to bluegreen-`${CI_COMMIT_SHORT_SHA}`. These variables are used throughout the deployment process to ensure that each deployment is unique.

During the deployment pipeline, a staging virtual service is applied that points the staging environment to the current deployment. This allows for testing of the new code in a production-like environment before it is released to the live environment.

For production, a "Go Live" job is run from the pipeline. This job applies a production virtual service that points the live environment to the current deployment. This allows for a seamless switch-over from the blue to the green environment, with zero downtime.
<br />
<br />

# Usage

To use this project, simply clone the repository and follow the instructions in the README.md file. Make sure to set up the necessary environment variables before deploying the project.
<br />
<br />

# Contributing
Contributions to this project are welcome! To contribute, simply fork the repository and create a pull request with your changes
<br />
<br />

# Authors
This project was created and maintained by `Ye Lin Aung`.